import { Moment } from 'moment-timezone';
export * from './utils/rmq';
export * from './utils/db';
export * from './utils/es';

export { auth } from './utils/auth';
export { cache } from './utils/cache';

export interface IError {
    error: {
        status: boolean;
        message: string;
        type?: string;
        data?: any;
    }
}

export interface IHuddlCategory {
    uuid?: string;
    title: string;
    subs?: IHuddlCategory[]
}

export interface IHuddlUser {
    uuid?: string;
    type?: string;
    category?: IHuddlCategory;
    username?: string;
    email?:string;
    password?: string;
    first_name?: string;
    last_name?: string;
    profile_pic?: string;
    complete?: boolean;
    verified?: boolean;
    reset_code?: string;
    complete_code?: string;
    tags?: ITag[];
}

export interface ITag {
    uuid: string;
    name: string;
}

export interface IHuddl {
    uuid?: string;
    id?: string;
    is_private: boolean;
    title: string;
    description?: string;
    start_date?: Moment;
    duration?: number;
    current_bookings?: number;
    max_bookings?: number;
    entertainer_id?: string;
    product_uuid?: string;
    price?: number;
    /// Future use...
    // subscribers_only?: boolean;
    // current?: {
    //     viewers?: number;
    //     video?: number;
    //     audio?: number;
    // };
    // likes?: number;
}

export interface IHuddlBookings {
    uuid?: string;
    host_id: string;
    booker_id: string;
    huddl_id: string;
    payment_id?: string;
}
