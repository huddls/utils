"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.rpcQueue = exports.loadDir = exports.send = exports.rpc = exports.register = exports.connect = exports.FAST = exports.NORMAL = exports.MID = exports.SLOW = exports.TURTLE = void 0;
var amqp = __importStar(require("amqplib"));
var uuid_1 = require("uuid");
var shortid = __importStar(require("shortid"));
var promise_queue_1 = __importDefault(require("promise-queue"));
var delay_1 = __importDefault(require("delay"));
var events_1 = require("events");
var fs = __importStar(require("fs"));
var maxConcurrent = 20;
var maxQueue = Infinity;
var pq = new promise_queue_1.default(maxConcurrent, maxQueue);
var tDelay = 100;
var MAX_RPC_QUEUES = process.env.MAX_RPC_QUEUES ? parseInt(process.env.MAX_RPC_QUEUES) : 10;
// Request timeouts
exports.TURTLE = 10000;
exports.SLOW = 5000;
exports.MID = 2500;
exports.NORMAL = 1000;
exports.FAST = 500;
var RABBIT_HOST;
var RABBIT_ENV;
var RABBIT_USER;
var RABBIT_PASSWORD;
var RABBIT_RETRY = 1;
var CURRENT_QUEUES = [];
var RPCEmitter = /** @class */ (function (_super) {
    __extends(RPCEmitter, _super);
    function RPCEmitter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return RPCEmitter;
}(events_1.EventEmitter));
var rpcEmitter = new RPCEmitter();
var BASE_NAME = process.env.QUEUE_BASE ? process.env.QUEUE_BASE : 'huddls';
var _channel;
var _connection;
var offlinePubQueue = [];
var closeOnErr = function (err) {
    if (!err)
        return false;
    console.error("[AMQP] error", err);
    _connection.close();
    return true;
};
var _connect = function (host, user, password, env) { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("Connecting to RabbitMQ");
                RABBIT_HOST = host;
                RABBIT_USER = user;
                RABBIT_PASSWORD = password;
                RABBIT_ENV = env;
                return [4 /*yield*/, amqp.connect({
                        protocol: 'amqp',
                        hostname: host,
                        port: 5672,
                        username: user,
                        password: password,
                        locale: 'en_US',
                        frameMax: 0,
                        heartbeat: 60,
                        vhost: env === 'prod' ? '/' : '/dev',
                    })
                        .then(function (connection) {
                        _connection = connection;
                        _connection.on("error", function (err) {
                            if (err.message !== "Connection closing") {
                                console.error("* [RabbitMQ] Connection Error", err.message);
                            }
                            else {
                                console.error("* [RabbitMQ] Error", err.message);
                            }
                        });
                        _connection.on("close", function () {
                            console.error("* [RabbitMQ] Reconnecting");
                            return setTimeout(function () {
                                exports.connect(RABBIT_HOST, RABBIT_USER, RABBIT_PASSWORD, RABBIT_ENV);
                            }, 1000);
                        });
                        return _connection;
                    })
                        .catch(function (error) {
                        console.error("[AMQP]", error.message);
                        setTimeout(function () {
                            exports.connect(RABBIT_HOST, RABBIT_USER, RABBIT_PASSWORD, RABBIT_ENV);
                        }, 1000);
                        return undefined;
                    })];
            case 1:
                result = _a.sent();
                return [2 /*return*/, result];
        }
    });
}); };
/* Create the AMQP Channel to Rabbit */
var _createChannel = function (connection) { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, connection.createConfirmChannel()
                    .then(function (channel) {
                    _channel = channel;
                    _channel.prefetch(10);
                    _channel.on('error', function (error) {
                        console.error("[AMQP] channel error", error.message);
                        setTimeout(function () {
                            _createChannel(_connection);
                        }, 1000);
                    });
                    _channel.on('close', function (error) {
                        console.log("[AMQP] channel closed");
                        setTimeout(function () {
                            _createChannel(_connection);
                        }, 1000);
                    });
                    // Handle queued
                    while (true) {
                        var m = offlinePubQueue.shift();
                        if (!m)
                            break;
                        _channel.publish(m[0], m[1], m[2], m[4]);
                    }
                    return _channel;
                })
                    .catch(function (error) {
                    console.log(error);
                    if (closeOnErr(error))
                        return undefined;
                    return undefined;
                })];
            case 1:
                result = _a.sent();
                return [2 /*return*/, result];
        }
    });
}); };
/* Assert the AMQP Exchange in Rabbit */
var _createExchange = function (channel) { return __awaiter(void 0, void 0, void 0, function () {
    var result, dlxExchange;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, channel.assertExchange(BASE_NAME, 'topic')
                    .catch(function (err) {
                    console.error(err);
                    return false;
                })];
            case 1:
                result = _a.sent();
                dlxExchange = channel.assertExchange(BASE_NAME + "-dlx", 'topic')
                    .catch(function (err) {
                    console.error(err);
                    return false;
                });
                return [2 /*return*/, result ? true : false];
        }
    });
}); };
/* Create and bind queues to AMQP Exchange in Rabbit */
/* Returns name of rpc reply queue */
var _createAndBindQueue = function (channel, newQueue, code) {
    if (code === void 0) { code = ""; }
    return __awaiter(void 0, void 0, void 0, function () {
        var success, name, rpcName, topic, bind, rpcQueue_1, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    success = true;
                    name = newQueue.name ? BASE_NAME + "-" + newQueue.name : undefined;
                    rpcName = generateRPCQueueID(code);
                    topic = newQueue.topic ? BASE_NAME + "." + newQueue.topic : undefined;
                    if (!(name && topic)) return [3 /*break*/, 5];
                    return [4 /*yield*/, (channel === null || channel === void 0 ? void 0 : channel.assertQueue(name, { deadLetterExchange: name + "-dlx", messageTtl: 30000 }).catch(function (err) {
                            console.error(err);
                            return false;
                        }))];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, (channel === null || channel === void 0 ? void 0 : channel.assertQueue(name + "-dlx").catch(function (err) {
                            console.error(err);
                            return false;
                        }))];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, (channel === null || channel === void 0 ? void 0 : channel.bindQueue(name, BASE_NAME, topic))];
                case 3:
                    bind = _a.sent();
                    if (!bind)
                        throw "Could not bind queue \"" + name + "\" to exchange \"" + BASE_NAME + "\"";
                    return [4 /*yield*/, (channel === null || channel === void 0 ? void 0 : channel.bindQueue(name + "-dlx", BASE_NAME + "-dlx", topic))];
                case 4:
                    bind = _a.sent();
                    if (!bind)
                        throw "Could not bind queue \"" + name + "-dlx\" to exchange \"" + BASE_NAME + "\"";
                    _a.label = 5;
                case 5:
                    _a.trys.push([5, 9, , 10]);
                    // Consume from topic if we have a consumer
                    if (newQueue.consumer && name)
                        channel === null || channel === void 0 ? void 0 : channel.consume(name, function (msg) {
                            if (!msg)
                                return;
                            if (newQueue.consumer)
                                newQueue.consumer(msg, channel);
                        });
                    // Set up an rpc queue if we have a callback
                    if (!newQueue.rpc)
                        return [2 /*return*/, undefined];
                    return [4 /*yield*/, (channel === null || channel === void 0 ? void 0 : channel.assertQueue(rpcName, { exclusive: true, autoDelete: true }).catch(function (err) {
                            console.log(err);
                            return false;
                        }))];
                case 6:
                    rpcQueue_1 = _a.sent();
                    if (!rpcQueue_1) return [3 /*break*/, 8];
                    return [4 /*yield*/, (channel === null || channel === void 0 ? void 0 : channel.consume(rpcName, function (msg) {
                            if (!msg)
                                return;
                            try {
                                rpcEmitter.emit(msg.properties.correlationId, JSON.parse(msg.content.toString()));
                            }
                            catch (error) {
                                rpcEmitter.emit(msg.properties.correlationId, { error: { status: true, message: "Server sent a bad response!" } });
                            }
                        }, { noAck: true }))];
                case 7:
                    _a.sent();
                    return [2 /*return*/, rpcName];
                case 8: return [3 /*break*/, 10];
                case 9:
                    error_1 = _a.sent();
                    console.error();
                    return [3 /*break*/, 10];
                case 10: return [2 /*return*/, undefined];
            }
        });
    });
};
exports.connect = function (host, user, password, env) { return __awaiter(void 0, void 0, void 0, function () {
    var connection, channel, exchange;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, _connect(host, user, password, env)];
            case 1:
                connection = _a.sent();
                if (!connection)
                    throw "Could not connect to RabbitMQ!";
                return [4 /*yield*/, _createChannel(connection)];
            case 2:
                channel = _a.sent();
                if (!channel)
                    throw "Could not create a channel with RabbitMQ!";
                return [4 /*yield*/, _createExchange(channel)];
            case 3:
                exchange = _a.sent();
                if (!exchange)
                    throw "Could not create the proper exchange with RabbitMQ!";
                console.log("RabbitMQ Connection and Exchange Validation Complete");
                console.log("[ MODULE REGISTRATION ENABLED ]");
                return [2 /*return*/];
        }
    });
}); };
/* Helper for correlationID */
var getCorrelationID = function () {
    return uuid_1.v4();
};
var generateRPCQueueID = function (code) {
    if (code === void 0) { code = ""; }
    return "rpc" + (code !== "" ? "-" + code : "") + "-" + shortid.generate() + "-" + shortid.generate();
};
/*
Register a function for RPC or receive queue.
This WILL wait for connection.

===== Example =====
rmq.register({
  name:'create',
  topic:'create',
  consumer:(msg, channel) => {
      
  },
  callback:(msg)=>{
    console.log(msg.content.toString());
  }
},false);
 */
exports.register = function (queue, code) {
    if (code === void 0) { code = ""; }
    return __awaiter(void 0, void 0, void 0, function () {
        var rpcQueueName;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, pq.add(function () { return __awaiter(void 0, void 0, void 0, function () {
                        var tBackoff, rpcChannel;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    tBackoff = 1;
                                    _a.label = 1;
                                case 1:
                                    if (!!_channel) return [3 /*break*/, 3];
                                    return [4 /*yield*/, delay_1.default(tDelay * tBackoff)];
                                case 2:
                                    _a.sent();
                                    tBackoff *= 2;
                                    return [3 /*break*/, 1];
                                case 3: return [4 /*yield*/, _createAndBindQueue(_channel, queue, code)];
                                case 4:
                                    rpcChannel = _a.sent();
                                    rpcChannel ? console.log("[RMQ]: Registering RPC queue " + rpcChannel + "...") : console.log("[RMQ]: Registering " + BASE_NAME + "." + queue.topic + " channel " + queue.name + "...");
                                    return [2 /*return*/, rpcChannel];
                            }
                        });
                    }); })];
                case 1:
                    rpcQueueName = _a.sent();
                    return [2 /*return*/, rpcQueueName];
            }
        });
    });
};
/*
*   Do an RPC Call
*/
exports.rpc = function (exchange, message, replyTo, timeout) {
    if (exchange === void 0) { exchange = BASE_NAME; }
    if (timeout === void 0) { timeout = exports.MID; }
    return __awaiter(void 0, void 0, void 0, function () {
        var correlationId, response, waiting, timedOut, t, rpcListener, rpcPromise;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    correlationId = message.correlationId ? message.correlationId : getCorrelationID();
                    try {
                        console.log("* [RMQ] Message: ", message);
                        console.log("* [RMQ] Data: ", message.data);
                        _channel.publish(exchange, message.topic ? message.topic : "", Buffer.from(JSON.stringify(message.data)), {
                            contentType: 'application/json',
                            correlationId: correlationId,
                            replyTo: replyTo ? replyTo : ""
                        });
                    }
                    catch (e) {
                        console.error("[AMQP] publish", e.message);
                        offlinePubQueue.push([
                            exchange,
                            message.topic ? message.topic : "",
                            Buffer.from(JSON.stringify(message.data)),
                            {
                                contentType: 'application/json',
                                correlationId: correlationId,
                                replyTo: replyTo ? replyTo : ""
                            }
                        ]);
                    }
                    waiting = true;
                    timedOut = false;
                    t = setTimeout(function () {
                        rpcEmitter.removeListener(correlationId, rpcListener);
                        waiting = false;
                        timedOut = true;
                    }, timeout);
                    rpcListener = function (data) {
                        clearTimeout(t);
                        response = data;
                    };
                    rpcEmitter.once(correlationId, rpcListener);
                    return [4 /*yield*/, (function () { return __awaiter(void 0, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!(!response && waiting)) return [3 /*break*/, 2];
                                        // Check 100x the wait timeout frequency...
                                        return [4 /*yield*/, delay_1.default(timeout / 100)];
                                    case 1:
                                        // Check 100x the wait timeout frequency...
                                        _a.sent();
                                        return [3 /*break*/, 0];
                                    case 2:
                                        if (timedOut)
                                            return [2 /*return*/, { error: { status: true, message: "Server request timed out." }, data: {} }];
                                        if (response)
                                            return [2 /*return*/, response];
                                        return [2 /*return*/, { error: { status: true, message: "An unknown error occurred." } }];
                                }
                            });
                        }); })()];
                case 1:
                    rpcPromise = _a.sent();
                    return [2 /*return*/, Promise.resolve(rpcPromise)];
            }
        });
    });
};
exports.send = function (message) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        _channel.publish(BASE_NAME, message.topic ? message.topic : "", Buffer.from(JSON.stringify(message.data)), {
            contentType: 'application/json',
        });
        return [2 /*return*/];
    });
}); };
/*
*   Folder loading!
*/
// Load up all operations and initialize them
exports.loadDir = function (path) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        fs.readdirSync(path).forEach(function (file) {
            (function () {
                return __awaiter(this, void 0, void 0, function () {
                    var op_1, name_1, args, i, topic, queue, rpcQueue_2, error_2;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (file == 'index.ts')
                                    return [2 /*return*/];
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 6, , 7]);
                                op_1 = require(path + "/" + file);
                                name_1 = file.replace('.ts', '');
                                name_1 = name_1.replace('.js', '');
                                if (!op_1.handleMessage) return [3 /*break*/, 5];
                                args = 1 + (op_1.MY_ARGS ? op_1.MY_ARGS.length : 0);
                                i = 0;
                                _a.label = 2;
                            case 2:
                                if (!(i < args)) return [3 /*break*/, 5];
                                topic = "" + name_1 + (i > 0 ? op_1.MY_ARGS ? ".*".repeat(i) : "" : "");
                                queue = {
                                    name: name_1,
                                    topic: topic,
                                    consumer: function (msg, channel) { return __awaiter(_this, void 0, void 0, function () {
                                        var args, replyTo, tArgs, i, response, message, success, error_3, err;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    console.log(JSON.stringify(msg));
                                                    args = _getArgs(msg.fields.routingKey, op_1.MY_ROOT, op_1);
                                                    replyTo = msg.properties.replyTo ? function (data) {
                                                        channel.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(data)), { correlationId: msg.properties.correlationId });
                                                    } : undefined;
                                                    if (args === undefined) {
                                                        tArgs = __spreadArrays(op_1.MY_ARGS);
                                                        for (i = 0; i < tArgs.length; i++)
                                                            tArgs[i] = "<" + tArgs[i] + ">";
                                                        response = {
                                                            error: {
                                                                status: true,
                                                                message: "Topic format should be " + op_1.MY_ROOT + "." + tArgs.join('.') + "}.",
                                                                data: {
                                                                    args: args
                                                                }
                                                            }
                                                        };
                                                        if (replyTo)
                                                            replyTo(response);
                                                        return [2 /*return*/, true];
                                                    }
                                                    _a.label = 1;
                                                case 1:
                                                    _a.trys.push([1, 3, , 4]);
                                                    message = JSON.parse(msg.content.toString());
                                                    return [4 /*yield*/, op_1.handleMessage(message, args, replyTo ? replyTo : undefined)];
                                                case 2:
                                                    success = _a.sent();
                                                    if (success)
                                                        return [2 /*return*/, channel === null || channel === void 0 ? void 0 : channel.ack(msg)];
                                                    return [3 /*break*/, 4];
                                                case 3:
                                                    error_3 = _a.sent();
                                                    err = {
                                                        error: {
                                                            status: true,
                                                            message: error_3,
                                                            type: "MessageHandler",
                                                            data: {
                                                                message: msg
                                                            }
                                                        }
                                                    };
                                                    if (replyTo)
                                                        replyTo(err);
                                                    return [2 /*return*/, channel === null || channel === void 0 ? void 0 : channel.ack(msg)];
                                                case 4:
                                                    if (msg.fields.redelivered)
                                                        return [2 /*return*/, channel.nack(msg, undefined, false)];
                                                    return [2 /*return*/, channel.nack(msg)];
                                            }
                                        });
                                    }); }
                                };
                                return [4 /*yield*/, exports.register(queue)];
                            case 3:
                                rpcQueue_2 = _a.sent();
                                if (rpcQueue_2 && op_1.setRpcQueue)
                                    op_1.setRpcQueue(rpcQueue_2);
                                _a.label = 4;
                            case 4:
                                i++;
                                return [3 /*break*/, 2];
                            case 5: return [3 /*break*/, 7];
                            case 6:
                                error_2 = _a.sent();
                                throw error_2;
                            case 7: return [2 /*return*/];
                        }
                    });
                });
            })();
        });
        return [2 /*return*/];
    });
}); };
var _getArgs = function (key, base_name, op) {
    var keys = key.replace(base_name + ".", '');
    var keyArgs = keys.split(".");
    var newArgs = {};
    if (keyArgs.length <= op.MY_ARGS.length) {
        for (var i = 0; i < keyArgs.length; i++) {
            newArgs[op.MY_ARGS[i]] = keyArgs[i];
        }
        return newArgs;
    }
    return undefined;
};
var rpcQueue = /** @class */ (function () {
    function rpcQueue(code, count) {
        var _this = this;
        if (count === void 0) { count = MAX_RPC_QUEUES; }
        this._queue = [];
        this._ready = false;
        (function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.register(code, count);
                return [2 /*return*/];
            });
        }); })();
    }
    rpcQueue.prototype.nextQueue = function () {
        var name = this._queue.shift();
        if (name)
            this._queue.push(name);
        return name ? name : "";
    };
    rpcQueue.prototype.isReady = function () {
        return this._ready;
    };
    rpcQueue.prototype.register = function (code, count) {
        return __awaiter(this, void 0, void 0, function () {
            var i, rpcChannel;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < count)) return [3 /*break*/, 4];
                        rpcChannel = undefined;
                        return [4 /*yield*/, exports.register({
                                rpc: true
                            }, code)];
                    case 2:
                        rpcChannel = _a.sent();
                        if (rpcChannel)
                            this._queue.push(rpcChannel);
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        this._ready = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    return rpcQueue;
}());
exports.rpcQueue = rpcQueue;
