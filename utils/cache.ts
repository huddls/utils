import redis, { RedisOptions } from 'ioredis';
import { promisify } from "util";

console.log("***** [REDIS] Connecting to: ", process.env.REDIS_SESSION_STRING)

let isConnected = false;

const redisClient = new redis(process.env.REDIS_SESSION_STRING,{
    connectTimeout: 10000,
    retryStrategy: function (times) {
        var delay = Math.min(times * 50, 2000);
        return delay;
    },
    reconnectOnError: ()=>true,
    lazyConnect: true
}); 

const getAsync = promisify(redisClient.get).bind(redisClient);

const MAX_CACHE = 3600; // 1 hour max

redisClient.on("error", function(error:string) {
    console.error(error);
});

redisClient.on("close", function(error: string){
    isConnected = false;
});

redisClient.on("end", function(error: string){
    isConnected = false;
});



redisClient.on("reconnecting", function(error: string){
    isConnected = false;
});

redisClient.on("connect", function(error:string) {
    console.log("Connected to Redis!");
    isConnected = true;
});

function isJson(str:any) {
    if(typeof str === "object")
        return true;
    try {
        JSON.parse(str);
        return true;
    } catch (e) {
        try {
            let keys = Object.keys(str);
            if(keys && keys.length > 0)
                return true;
        } catch (e) {
            return false;
        }
        return false;
    }
    return false;
}

export const cache = {
    get: async function(key:string):Promise<any> {
        // if(!isConnected)
        //     return null;
        console.log('Looking for '+key);
        try {
	        var result:any = await redisClient.get(key).catch(console.error);
	        if(result){
	            try {
	                return JSON.parse(result);
	            } catch (e) {
                    console.log("Returning: ", result);
                    return result;
	            }
	        } 
        } catch (error) {
        	console.error(error);
        }
        return null;
    },
    
    set: function(key:string, value:any, expire:number=MAX_CACHE):void {
        // if(!isConnected)
        //     return;
        try {
            if(isJson(value))
                value = JSON.stringify(value);
        } catch (e) {
            return;
        }
        try {
	        redisClient.setex(key, expire, value).catch(console.error);
        } catch(error) {
        	console.error(error);
        }
    },

    setExpire: function(key:string, time:number):void
    {
        // if(!isConnected)
        //     return;
    	try {
            redisClient.expire(key, time);
        } catch (error) {
        	console.error(error);
        }
    },

    deleteAllLike: function(key:string):void
    {
        let stream = redisClient.scanStream(
            {
                match: key,
                count: 100
            }
        );
        stream.on("data", (resultKeys) => {
            // Pause the stream from scanning more keys until we've migrated the current keys.
            stream.pause();
          
            Promise.all(resultKeys.map((k:string)=>{
                console.log(k);
                this.set(k,null,1);
            })).then(() => {
              // Resume the stream here.
              stream.resume();
            });
          });
          
          stream.on("end", () => {
            console.log("Cache delete done for: ",key);
          });
    },

    delete: function(key:string):void {
        // if(!isConnected)
        //     return;
    	try {
            redisClient.del(key,function(err,result){
                if (err) {
                    console.error(err);
                    return;
                }
            });
        } catch (error) {
        	console.error(error);
        }
    }
}