import { IError } from '../interfaces';

import {
    Sequelize,
    BaseError,
    ValidationError,
    DatabaseError,
    TimeoutError, 
    UniqueConstraintError, 
    ForeignKeyConstraintError, 
    ExclusionConstraintError, 
    ConnectionError,
    ConnectionRefusedError,
    AccessDeniedError,
    ConnectionTimedOutError
} from 'sequelize-cockroachdb';
import * as fs from 'fs';

const DB_NAME = process.env.HUDDLS_DB_NAME ? process.env.HUDDLS_DB_NAME : "";
const DB_USER = process.env.HUDDLS_DB_USER ? process.env.HUDDLS_DB_USER : "";
const DB_HOST = process.env.HUDDLS_DB_HOST ? process.env.HUDDLS_DB_HOST : "";
const DB_PORT = process.env.HUDDLS_DB_PORT ? parseInt(process.env.HUDDLS_DB_PORT) : 10000;

const DB_CA_CERT = process.env.HUDDLS_DB_CA_CERT ? process.env.HUDDLS_DB_CA_CERT : "";
const DB_USER_KEY = process.env.HUDDLS_DB_USER_KEY ? process.env.HUDDLS_DB_USER_KEY : "";
const DB_USER_CERT = process.env.HUDDLS_DB_USER_CERT ? process.env.HUDDLS_DB_USER_CERT : "";

// Connect to CockroachDB through Sequelize.
export const db:Sequelize = new Sequelize(DB_NAME, DB_USER, '', {
    dialect: 'postgres',
    host: DB_HOST,
    port: DB_PORT,
    logging: console.log,
    logQueryParameters: true,
    pool: {
        max: 24,
        min: 4,
        acquire: 60000,
        idle: 10000
    },
    dialectOptions: {
        ssl: {
            ca: fs.readFileSync(DB_CA_CERT)
                .toString(),
            key: fs.readFileSync(DB_USER_KEY)
                .toString(),
            cert: fs.readFileSync(DB_USER_CERT)
                .toString(),
            rejectUnauthorized: true
        }
    }
});

/*
*   Fun with errors! But seriously... let's take those shitty errors and make them better.
*/
// Gentle unkonwn error handler
export const unknownError = (file:string, data?: any): IError => {
    return {
        error: {
            status: true,
            message: `An nknown error has occurred in ${file}.`,
            type: "UnkownError",
            data
        }
    }
}
// Prettify the DB errors. Cause they suck.
export const handleDbError = (error: any): IError => {
    let prettyError: IError = {
        error: {
            status: true,
            message: error.message,
            data: {
                fields: error.fields,
                sql: error.sql,
                value: error.value,
                index: error.index,
                parent: error.parent
            }
        }
    };

    switch(error.constructor)
    {
        case BaseError:
            prettyError.error.type = "BaseError";
            break;
        case ValidationError: 
            prettyError.error.type = "ValidationError";
            break;
        case DatabaseError: 
            prettyError.error.type = "DatabaseError";
            break;
        case TimeoutError:
            prettyError.error.type = "TimeoutError";
            break;
        case UniqueConstraintError: 
            prettyError.error.type = "UniqueConstraintError";
            break;
        case ForeignKeyConstraintError:
            prettyError.error.type = "ForeignKeyConstraintError";
            break;
        case ExclusionConstraintError:
            prettyError.error.type = "ExclusionConstraintError";
            break;
        case ConnectionError:
            prettyError.error.type = "ConnectionError";
            break;
        case ConnectionRefusedError:
            prettyError.error.type = "ConnectionRefusedError";
            break;
        case AccessDeniedError:
            prettyError.error.type = "AccessDeniedError";
            break;
        case ConnectionTimedOutError:
            prettyError.error.type = "ConnectionTimedOutError";
            break;
        default: 
            break;
    }

    return prettyError;
}