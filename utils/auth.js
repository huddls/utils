"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.auth = void 0;
var bcrypt = __importStar(require("bcrypt"));
var jwt = __importStar(require("jsonwebtoken"));
var moment_timezone_1 = __importDefault(require("moment-timezone"));
var shortid = __importStar(require("shortid"));
var cache_1 = require("./cache");
var saltRounds = 10;
var codeLength = 32;
var codeType = 'base64';
/// TS Secret
var HUDDLS_JWT_SECRET = process.env.HUDDLS_JWT_SECRET;
var JITSI_JWT_SECRET = process.env.JITSI_JWT_SECRET;
var Authentication = /** @class */ (function () {
    function Authentication() {
    }
    Authentication.prototype.validateLoginToken = function (token) {
        try {
            var data = jwt.verify(token, HUDDLS_JWT_SECRET);
            if (data) {
                if (parseInt(data.exp) >= moment_timezone_1.default().unix() && data.type !== "request")
                    return data;
                console.log("Token expired?", data);
                console.log("Moment Time: ", moment_timezone_1.default().unix());
                console.log("My Exp: ", parseInt(data.exp));
            }
        }
        catch (error) {
            console.log(error);
        }
        return null;
    };
    // Refresh token
    Authentication.prototype.refreshLoginToken = function (refresh_token) {
        return __awaiter(this, void 0, void 0, function () {
            var data, uuid, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        data = jwt.verify(refresh_token, HUDDLS_JWT_SECRET);
                        console.log(data);
                        if (!data.token) return [3 /*break*/, 2];
                        return [4 /*yield*/, cache_1.cache.get("_refresh/" + data.token)];
                    case 1:
                        uuid = _a.sent();
                        if (uuid) {
                            cache_1.cache.delete("_refresh/" + data.token);
                            return [2 /*return*/, { uuid: uuid, error: null }];
                        }
                        else {
                            console.log("Could not find refresh token in cache...");
                            return [2 /*return*/, { error: "Unable to get refresh token" }];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        console.log("Invalid token, no token.");
                        return [2 /*return*/, { error: "Invalid Refresh Token" }];
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log("Invalid token: ", error_1);
                        return [2 /*return*/, { error: "Invalid Refresh Token" }];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    Authentication.prototype.createLoginToken = function (user) {
        try {
            // This seems wrong... someone should fix it.
            var refreshToken = shortid.generate() + "." + shortid.generate() + "." + shortid.generate();
            cache_1.cache.set("_refresh/" + refreshToken, "" + user.uuid, 2592000000);
            var refresh_data = {
                token: refreshToken,
                type: "refresh",
                iss: "huddls.com",
                aud: "huddls",
                sub: "user",
                exp: moment_timezone_1.default().unix() + 2592000,
                auth_time: moment_timezone_1.default().unix()
            };
            var data = {
                uuid: user.uuid,
                type: user.type,
                iss: "huddls.com",
                aud: "huddls",
                sub: "user",
                exp: moment_timezone_1.default().unix() + 900,
                auth_time: moment_timezone_1.default().unix()
            };
            return { token: jwt.sign(data, HUDDLS_JWT_SECRET, {}), refresh: jwt.sign(refresh_data, HUDDLS_JWT_SECRET) };
        }
        catch (error) {
            console.error(error);
        }
    };
    Authentication.prototype.decodeCommonJWT = function (token) {
        try {
            var data = jwt.verify(token, HUDDLS_JWT_SECRET);
            return data.payload;
        }
        catch (error) {
            console.log(error);
            return null;
        }
    };
    Authentication.prototype.decodeActionToken = function (token) {
        try {
            var data = jwt.verify(token, HUDDLS_JWT_SECRET);
            return data;
        }
        catch (error) {
            console.log(error);
            return null;
        }
    };
    Authentication.prototype.encodeActionToken = function (action, type, payload) {
        try {
            var token = jwt.sign({ action: action, type: type, payload: payload }, HUDDLS_JWT_SECRET);
            return token;
        }
        catch (error) {
            console.log(error);
            return null;
        }
    };
    Authentication.prototype.encodeCommonJWT = function (payload) {
        try {
            var token = jwt.sign({ payload: payload }, HUDDLS_JWT_SECRET);
            return token;
        }
        catch (error) {
            console.log(error);
            return null;
        }
    };
    Authentication.prototype.generateMeetToken = function (user, room, moderator, expires) {
        if (expires === void 0) { expires = null; }
        var data = {
            context: {
                user: {
                    avatar: "https://static.huddls.com" + user.profile_pic,
                    name: user.first_name + " " + user.last_name,
                    id: user.uuid
                },
                group: user.type
            },
            moderator: moderator,
            aud: "meet.huddls.com",
            iss: "meet.huddls.com",
            sub: "meet.huddls.com",
            room: room,
            expires: moment_timezone_1.default().add(12, 'hours').unix()
        };
        console.log(data);
        var token = jwt.sign(data, JITSI_JWT_SECRET);
        return token;
    };
    Authentication.prototype.generateHash = function (password) {
        return __awaiter(this, void 0, void 0, function () {
            var hash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, bcrypt.hash(password, saltRounds)];
                    case 1:
                        hash = _a.sent();
                        return [2 /*return*/, hash];
                }
            });
        });
    };
    Authentication.prototype.checkHash = function (clearPassword, hashedPassword) {
        return __awaiter(this, void 0, void 0, function () {
            var success;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, bcrypt.compare(clearPassword, hashedPassword)];
                    case 1:
                        success = _a.sent();
                        return [2 /*return*/, success];
                }
            });
        });
    };
    Authentication.prototype.generateCode = function () {
        return shortid.generate() + "." + shortid.generate();
    };
    return Authentication;
}());
exports.auth = new Authentication();
