"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cleanData = exports.es = void 0;
/* ES Connection */
var elasticsearch_1 = require("@elastic/elasticsearch");
var es;
exports.es = es;
try {
    exports.es = es = new elasticsearch_1.Client({ node: process.env.ELASTICSEARCH_URL });
}
catch (error) {
    console.log("ElasticSearch config not found. Will be unavailable!");
    exports.es = es = undefined;
}
// Safety ! Remove PII or contact
var removeUnsafe = function (obj) {
    delete (obj.password);
    delete (obj.reset_code);
    //delete(obj.email);
    delete (obj.complete_code);
    return obj;
};
// Clean data
var cleanData = function (obj) {
    Object.keys(obj).forEach(function (k) {
        return (obj[k] && typeof obj[k] === 'object') && cleanData(obj[k]) ||
            (!obj[k] && obj[k] !== undefined) && delete obj[k];
    });
    obj = removeUnsafe(obj);
    return obj;
};
exports.cleanData = cleanData;
