import * as amqp from 'amqplib';
import { v4 as uuidv4 } from 'uuid';
import * as shortid from 'shortid';
import Queue from 'promise-queue';
import delay from 'delay';
import { EventEmitter }  from 'events'; 
import { IError } from '../interfaces';
import * as fs from 'fs';

const maxConcurrent = 20;
const maxQueue = Infinity;
const pq = new Queue(maxConcurrent, maxQueue);
const tDelay = 100;
const MAX_RPC_QUEUES = process.env.MAX_RPC_QUEUES ? parseInt(process.env.MAX_RPC_QUEUES) as number : 10;

// Request timeouts
export const TURTLE = 10000;
export const SLOW = 5000;
export const MID = 2500;
export const NORMAL = 1000;
export const FAST = 500;

let RABBIT_HOST:string;
let RABBIT_ENV:string;
let RABBIT_USER:string;
let RABBIT_PASSWORD:string;
let RABBIT_RETRY:number = 1;
const CURRENT_QUEUES = [];

class RPCEmitter extends EventEmitter {}
const rpcEmitter = new RPCEmitter();

let BASE_NAME = process.env.QUEUE_BASE ? process.env.QUEUE_BASE : 'huddls';
let _channel:amqp.ConfirmChannel;
let _connection:amqp.Connection;
let offlinePubQueue:any = [];

//// Export interfaces
export interface IQueueHandler {
    (data:any, args:any|undefined, replyTo:any|undefined):Promise<boolean>;
}
export interface IQueueRouter {
    (msg:amqp.ConsumeMessage, channel:amqp.Channel):any;
}
export interface IQueue {
	name?: string;
	topic?: string;
    consumer?: IQueueRouter;
    // Can queue with ONLY rpc 
    rpc?: true;
}
export interface IQueueMessage {
    topic?: string;
    correlationId?: string;
    data: any;
}

const closeOnErr = (err:any) => {
    if (!err) return false;
    console.error("[AMQP] error", err);
    _connection.close();
    return true;
  }

const _connect = async(host: string, user: string, password: string, env: string):Promise<amqp.Connection | undefined> => {
    console.log(`Connecting to RabbitMQ`);
    RABBIT_HOST = host;
    RABBIT_USER = user;
    RABBIT_PASSWORD = password;
    RABBIT_ENV = env;
    let result = await amqp.connect({
        protocol: 'amqp',
        hostname: host,
        port: 5672,
        username: user,
        password: password,
        locale: 'en_US',
        frameMax: 0,
        heartbeat: 60,
        vhost: env === 'prod' ? '/' : '/dev',
    })
    .then(connection=>{
        _connection = connection;

        _connection.on("error", function(err) {
            if (err.message !== "Connection closing") {
                console.error("* [RabbitMQ] Connection Error", err.message);
            } else {
                console.error("* [RabbitMQ] Error", err.message);
            }
        });

        _connection.on("close", function() {
            console.error("* [RabbitMQ] Reconnecting");
            return setTimeout(()=>{
                connect(RABBIT_HOST, RABBIT_USER, RABBIT_PASSWORD, RABBIT_ENV);
            }, 1000);
        });
        return _connection;
    })
    .catch(error => {
        console.error("[AMQP]", error.message);
        setTimeout(()=>{
                connect(RABBIT_HOST, RABBIT_USER, RABBIT_PASSWORD, RABBIT_ENV);
            }, 1000);
        return undefined;
    });
    return result;
}

/* Create the AMQP Channel to Rabbit */
const _createChannel = async(connection:amqp.Connection):Promise<amqp.Channel | undefined> => {
    let result = await connection.createConfirmChannel()
        .then(channel => {
            _channel = channel;
            _channel.prefetch(10);
            _channel.on('error',(error)=>{
                console.error("[AMQP] channel error", error.message);
                setTimeout(()=>{
                    _createChannel(_connection);
                },1000);
            });
            _channel.on('close',(error)=>{
                console.log("[AMQP] channel closed");
                setTimeout(()=>{
                    _createChannel(_connection);
                },1000);
            });

            // Handle queued
            while (true) {
                var m = offlinePubQueue.shift();
                if (!m) break;
                _channel.publish(m[0], m[1], m[2], m[4]);
            }

            return _channel;
        })
        .catch(error => {
            console.log(error);
            if (closeOnErr(error)) return undefined;
            return undefined;
        });
    return result;
}
/* Assert the AMQP Exchange in Rabbit */
const _createExchange = async(channel:amqp.Channel):Promise<boolean> => {
    let result = await channel.assertExchange(BASE_NAME,'topic')
        .catch(err=>{
            console.error(err);
            return false;
        });
    let dlxExchange = channel.assertExchange(`${BASE_NAME}-dlx`,'topic')
        .catch(err=>{
            console.error(err);
            return false;
        });
    return result ? true : false;
}
/* Create and bind queues to AMQP Exchange in Rabbit */
/* Returns name of rpc reply queue */
const _createAndBindQueue = async(channel:amqp.Channel | undefined, newQueue:IQueue, code:string=""):Promise<string | undefined> => {
    let success = true;

    let name = newQueue.name ? `${BASE_NAME}-${newQueue.name}` : undefined;
    let rpcName = generateRPCQueueID(code);
    let topic = newQueue.topic ? `${BASE_NAME}.${newQueue.topic}` : undefined;
    if(name && topic)
    {
        await channel?.assertQueue(name,{deadLetterExchange:`${name}-dlx`, messageTtl:30000})
        .catch(err => {
            console.error(err);
            return false;
        });
        await channel?.assertQueue(`${name}-dlx`)
        .catch(err => {
            console.error(err);
            return false;
        });
        let bind = await channel?.bindQueue(name,BASE_NAME,topic);
        if(!bind)
            throw `Could not bind queue "${name}" to exchange "${BASE_NAME}"`;
        bind = await channel?.bindQueue(`${name}-dlx`,`${BASE_NAME}-dlx`,topic);
        if(!bind)
            throw `Could not bind queue "${name}-dlx" to exchange "${BASE_NAME}"`;
    }

    // Set up consumers
    try {
        // Consume from topic if we have a consumer
        if(newQueue.consumer && name)
            channel?.consume(name, (msg:amqp.ConsumeMessage | null) => {
                if(!msg)
                    return;
                if(newQueue.consumer)
                    newQueue.consumer(msg,channel);
            });
        // Set up an rpc queue if we have a callback
        if(!newQueue.rpc)
            return undefined;
        let rpcQueue = await channel?.assertQueue(rpcName, {exclusive:true, autoDelete: true})
            .catch(err => {
                console.log(err);
                return false;
            }); 
        if(rpcQueue)
        {
            await channel?.consume(rpcName, (msg:amqp.ConsumeMessage|null)=>{
                if(!msg)
                    return;
                try {
                    rpcEmitter.emit(msg.properties.correlationId, JSON.parse(msg.content.toString()));
                } catch(error) {
                    rpcEmitter.emit(msg.properties.correlationId, {error:{status:true, message:"Server sent a bad response!"}});
                }
            },{noAck:true});        
            return rpcName;
        }
    } catch(error) { 
        console.error() 
    }
    return undefined;
}

export const connect = async(host: string, user: string, password: string, env: string):Promise<void> => {
    let connection = await _connect(host, user, password, env);
    if(!connection)
        throw "Could not connect to RabbitMQ!";

    let channel = await _createChannel(connection);
    if(!channel)
        throw "Could not create a channel with RabbitMQ!";

    let exchange = await _createExchange(channel);
    if(!exchange)
        throw "Could not create the proper exchange with RabbitMQ!";

    console.log("RabbitMQ Connection and Exchange Validation Complete");
    console.log("[ MODULE REGISTRATION ENABLED ]");
}

/* Helper for correlationID */
const getCorrelationID = ():string => {
    return uuidv4();
}

const generateRPCQueueID = (code:string=""):string=>{
    return `rpc${code!=="" ? `-${code}` : ""}-${shortid.generate()}-${shortid.generate()}`;
}

/* 
Register a function for RPC or receive queue.
This WILL wait for connection.

===== Example =====
rmq.register({
  name:'create', 
  topic:'create', 
  consumer:(msg, channel) => {
      
  },
  callback:(msg)=>{
    console.log(msg.content.toString());
  }
},false);
 */
export const register = async(queue: IQueue, code:string=""):Promise<string | undefined> => {
    let rpcQueueName =  await pq.add(async () => {
        let tBackoff = 1;
        while(!_channel)
        {
            await delay(tDelay*tBackoff);
            tBackoff *= 2;
        }
        let rpcChannel = await _createAndBindQueue(_channel,queue,code);
        rpcChannel ? console.log(`[RMQ]: Registering RPC queue ${rpcChannel}...`) : console.log(`[RMQ]: Registering ${BASE_NAME}.${queue.topic} channel ${queue.name}...`); 
        return rpcChannel;
    });
    return rpcQueueName;
}

/*
*   Do an RPC Call
*/
export const rpc = async(exchange:string=BASE_NAME, message: IQueueMessage, replyTo: string, timeout:number=MID):Promise<IQueueMessage|IError> => {
    let correlationId:string = message.correlationId ? message.correlationId : getCorrelationID();
    // Setup listener
    let response:IQueueMessage|IError|undefined;
    try {
        console.log("* [RMQ] Message: ",message);
        console.log("* [RMQ] Data: ", message.data);
        _channel.publish(
            exchange,
            message.topic ? message.topic : "", 
            Buffer.from(JSON.stringify(message.data)),
            {
                contentType: 'application/json',
                correlationId: correlationId,
                replyTo: replyTo ? replyTo : ""
            }   
        );
    } catch(e) {
        console.error("[AMQP] publish", e.message);
        offlinePubQueue.push([
            exchange, 
            message.topic ? message.topic : "",
            Buffer.from(JSON.stringify(message.data)),
            {
                contentType: 'application/json',
                correlationId: correlationId,
                replyTo: replyTo ? replyTo : ""
            }  
        ]);
    }
    let waiting = true;
    let timedOut = false;
    let t = setTimeout(()=>{
        rpcEmitter.removeListener(correlationId, rpcListener);
        waiting = false;
        timedOut = true;
    }, timeout);
    const rpcListener = (data:IQueueMessage) => {
        clearTimeout(t);
        response = data;
    };
    rpcEmitter.once(correlationId,rpcListener);  
    let rpcPromise = await (async():Promise<IQueueMessage | IError>=>{
        while(!response && waiting)
        {   
            // Check 100x the wait timeout frequency...
            await delay(timeout/100);
        }
        if(timedOut)
            return {error: {status: true, message: "Server request timed out."},data:{}};
        if(response)
            return response;
        return {error: {status: true, message: "An unknown error occurred."}};
    })();
    return Promise.resolve(rpcPromise);
}

export const send = async(message: IQueueMessage) => {
    _channel.publish(
        BASE_NAME,
        message.topic ? message.topic : "", 
        Buffer.from(JSON.stringify(message.data)),
        {
            contentType: 'application/json',
        }
    );
}

/*
*   Folder loading! 
*/
// Load up all operations and initialize them
export const loadDir = async(path:string):Promise<void> => {
    fs.readdirSync(path).forEach(function(file:string) {
    (async function(){
        if(file=='index.ts')
            return; 
        try {
            let op = require(`${path}/${file}`);
            let name = file.replace('.ts','');
            name = name.replace('.js', '');
            if(op.handleMessage) {
                /* Validate and build useful args */
                let args = 1 + (op.MY_ARGS ? op.MY_ARGS.length : 0);
                
                for(var i=0; i<args; i++) 
                {
                    let topic = `${name}${i>0 ? op.MY_ARGS ? ".*".repeat(i) : "" : ""}`;
                    let queue:IQueue = {
                        name, 
                        topic,
                        consumer:async(msg, channel) => {
                            console.log(JSON.stringify(msg));
                            let args = _getArgs(msg.fields.routingKey, op.MY_ROOT, op);

                            let replyTo = msg.properties.replyTo ? (data:IQueueMessage | IError) => {
                                channel.sendToQueue(msg.properties.replyTo,Buffer.from(JSON.stringify(data)),{correlationId: msg.properties.correlationId});
                            } : undefined;

                            if(args === undefined)
                            {
                                let tArgs = [...op.MY_ARGS];
                                for(var i=0; i<tArgs.length; i++)
                                tArgs[i] = `<${tArgs[i]}>`
                                let response = {
                                    error: {
                                        status: true,
                                        message: `Topic format should be ${op.MY_ROOT}.${tArgs.join('.')}}.`,
                                        data: {
                                            args
                                        }
                                    }
                                }
                                if(replyTo)
                                    replyTo(response);
                                return true;
                            }
                            try {
                                let message = JSON.parse(msg.content.toString());
                                let success = await op.handleMessage(message, args, replyTo ? replyTo : undefined);
                                if(success)
                                    return channel?.ack(msg); 
                            } catch (error) {
                                /// Bad message; Ack, log, fail;
                                let err:IError = {
                                    error: {
                                        status: true,
                                        message: error,
                                        type: "MessageHandler",
                                        data: {
                                            message: msg
                                        }
                                    }
                                }
                                if(replyTo)
                                    replyTo(err);
                                return channel?.ack(msg);
                            }
                            if(msg.fields.redelivered)
                                return channel.nack(msg,undefined,false);
                            return channel.nack(msg);
                        }
                    }
                    let rpcQueue = await register(queue); // Won't be set even if it's here.
                    if(rpcQueue && op.setRpcQueue)
                        op.setRpcQueue(rpcQueue);
                }
            }
        } catch(error) {
            throw error;
        }
    })();
    });   
}

const _getArgs = (key:string, base_name:string, op:any):any | undefined => {
    let keys = key.replace (`${base_name}.`,'');
    let keyArgs:string[] = keys.split(".");
    let newArgs:any = {};
    if(keyArgs.length <= op.MY_ARGS.length)
    {
        for(var i=0; i<keyArgs.length; i++)
        {
            newArgs[op.MY_ARGS[i]] = keyArgs[i];
        }
        return newArgs;
    }
    return undefined;
};

export class rpcQueue {
    private _queue: string[] = [];
    private _ready: boolean = false;

    constructor(code:string, count:number=MAX_RPC_QUEUES)
    {
        (async()=>{
            this.register(code, count);
        })();
    }

    public nextQueue():string
    {
        let name:string | undefined = this._queue.shift(); 
        if(name)   
            this._queue.push(name);
        return name ? name : "";
    }

    public isReady()
    {
        return this._ready;
    }

    private async register(code:string, count:number) {
        for(let i=0; i<count; i++)
        {
            let rpcChannel: string | undefined = undefined;
            rpcChannel = await register({
                rpc: true
            },code);
            if(rpcChannel)
                this._queue.push(rpcChannel);
        }
        this._ready = true;
    }
}