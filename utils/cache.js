"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cache = void 0;
var ioredis_1 = __importDefault(require("ioredis"));
var util_1 = require("util");
console.log("***** [REDIS] Connecting to: ", process.env.REDIS_SESSION_STRING);
var isConnected = false;
var redisClient = new ioredis_1.default(process.env.REDIS_SESSION_STRING, {
    connectTimeout: 10000,
    retryStrategy: function (times) {
        var delay = Math.min(times * 50, 2000);
        return delay;
    },
    reconnectOnError: function () { return true; },
    lazyConnect: true
});
var getAsync = util_1.promisify(redisClient.get).bind(redisClient);
var MAX_CACHE = 3600; // 1 hour max
redisClient.on("error", function (error) {
    console.error(error);
});
redisClient.on("close", function (error) {
    isConnected = false;
});
redisClient.on("end", function (error) {
    isConnected = false;
});
redisClient.on("reconnecting", function (error) {
    isConnected = false;
});
redisClient.on("connect", function (error) {
    console.log("Connected to Redis!");
    isConnected = true;
});
function isJson(str) {
    if (typeof str === "object")
        return true;
    try {
        JSON.parse(str);
        return true;
    }
    catch (e) {
        try {
            var keys = Object.keys(str);
            if (keys && keys.length > 0)
                return true;
        }
        catch (e) {
            return false;
        }
        return false;
    }
    return false;
}
exports.cache = {
    get: function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // if(!isConnected)
                        //     return null;
                        console.log('Looking for ' + key);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, redisClient.get(key).catch(console.error)];
                    case 2:
                        result = _a.sent();
                        if (result) {
                            try {
                                return [2 /*return*/, JSON.parse(result)];
                            }
                            catch (e) {
                                console.log("Returning: ", result);
                                return [2 /*return*/, result];
                            }
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, null];
                }
            });
        });
    },
    set: function (key, value, expire) {
        if (expire === void 0) { expire = MAX_CACHE; }
        // if(!isConnected)
        //     return;
        try {
            if (isJson(value))
                value = JSON.stringify(value);
        }
        catch (e) {
            return;
        }
        try {
            redisClient.setex(key, expire, value).catch(console.error);
        }
        catch (error) {
            console.error(error);
        }
    },
    setExpire: function (key, time) {
        // if(!isConnected)
        //     return;
        try {
            redisClient.expire(key, time);
        }
        catch (error) {
            console.error(error);
        }
    },
    deleteAllLike: function (key) {
        var _this = this;
        var stream = redisClient.scanStream({
            match: key,
            count: 100
        });
        stream.on("data", function (resultKeys) {
            // Pause the stream from scanning more keys until we've migrated the current keys.
            stream.pause();
            Promise.all(resultKeys.map(function (k) {
                console.log(k);
                _this.set(k, null, 1);
            })).then(function () {
                // Resume the stream here.
                stream.resume();
            });
        });
        stream.on("end", function () {
            console.log("Cache delete done for: ", key);
        });
    },
    delete: function (key) {
        // if(!isConnected)
        //     return;
        try {
            redisClient.del(key, function (err, result) {
                if (err) {
                    console.error(err);
                    return;
                }
            });
        }
        catch (error) {
            console.error(error);
        }
    }
};
