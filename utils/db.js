"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleDbError = exports.unknownError = exports.db = void 0;
var sequelize_cockroachdb_1 = require("sequelize-cockroachdb");
var fs = __importStar(require("fs"));
var DB_NAME = process.env.HUDDLS_DB_NAME ? process.env.HUDDLS_DB_NAME : "";
var DB_USER = process.env.HUDDLS_DB_USER ? process.env.HUDDLS_DB_USER : "";
var DB_HOST = process.env.HUDDLS_DB_HOST ? process.env.HUDDLS_DB_HOST : "";
var DB_PORT = process.env.HUDDLS_DB_PORT ? parseInt(process.env.HUDDLS_DB_PORT) : 10000;
var DB_CA_CERT = process.env.HUDDLS_DB_CA_CERT ? process.env.HUDDLS_DB_CA_CERT : "";
var DB_USER_KEY = process.env.HUDDLS_DB_USER_KEY ? process.env.HUDDLS_DB_USER_KEY : "";
var DB_USER_CERT = process.env.HUDDLS_DB_USER_CERT ? process.env.HUDDLS_DB_USER_CERT : "";
// Connect to CockroachDB through Sequelize.
exports.db = new sequelize_cockroachdb_1.Sequelize(DB_NAME, DB_USER, '', {
    dialect: 'postgres',
    host: DB_HOST,
    port: DB_PORT,
    logging: console.log,
    logQueryParameters: true,
    pool: {
        max: 24,
        min: 4,
        acquire: 60000,
        idle: 10000
    },
    dialectOptions: {
        ssl: {
            ca: fs.readFileSync(DB_CA_CERT)
                .toString(),
            key: fs.readFileSync(DB_USER_KEY)
                .toString(),
            cert: fs.readFileSync(DB_USER_CERT)
                .toString(),
            rejectUnauthorized: true
        }
    }
});
/*
*   Fun with errors! But seriously... let's take those shitty errors and make them better.
*/
// Gentle unkonwn error handler
exports.unknownError = function (file, data) {
    return {
        error: {
            status: true,
            message: "An nknown error has occurred in " + file + ".",
            type: "UnkownError",
            data: data
        }
    };
};
// Prettify the DB errors. Cause they suck.
exports.handleDbError = function (error) {
    var prettyError = {
        error: {
            status: true,
            message: error.message,
            data: {
                fields: error.fields,
                sql: error.sql,
                value: error.value,
                index: error.index,
                parent: error.parent
            }
        }
    };
    switch (error.constructor) {
        case sequelize_cockroachdb_1.BaseError:
            prettyError.error.type = "BaseError";
            break;
        case sequelize_cockroachdb_1.ValidationError:
            prettyError.error.type = "ValidationError";
            break;
        case sequelize_cockroachdb_1.DatabaseError:
            prettyError.error.type = "DatabaseError";
            break;
        case sequelize_cockroachdb_1.TimeoutError:
            prettyError.error.type = "TimeoutError";
            break;
        case sequelize_cockroachdb_1.UniqueConstraintError:
            prettyError.error.type = "UniqueConstraintError";
            break;
        case sequelize_cockroachdb_1.ForeignKeyConstraintError:
            prettyError.error.type = "ForeignKeyConstraintError";
            break;
        case sequelize_cockroachdb_1.ExclusionConstraintError:
            prettyError.error.type = "ExclusionConstraintError";
            break;
        case sequelize_cockroachdb_1.ConnectionError:
            prettyError.error.type = "ConnectionError";
            break;
        case sequelize_cockroachdb_1.ConnectionRefusedError:
            prettyError.error.type = "ConnectionRefusedError";
            break;
        case sequelize_cockroachdb_1.AccessDeniedError:
            prettyError.error.type = "AccessDeniedError";
            break;
        case sequelize_cockroachdb_1.ConnectionTimedOutError:
            prettyError.error.type = "ConnectionTimedOutError";
            break;
        default:
            break;
    }
    return prettyError;
};
