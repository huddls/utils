/* ES Connection */
import { Client } from '@elastic/elasticsearch';
let es:Client|undefined;
try {
    es = new Client({ node: process.env.ELASTICSEARCH_URL });
} catch(error) {
    console.log("ElasticSearch config not found. Will be unavailable!");
    es = undefined;
}

// Safety ! Remove PII or contact
const removeUnsafe = (obj:any):any => {
    delete(obj.password);
    delete(obj.reset_code);
    //delete(obj.email);
    delete(obj.complete_code);
    return obj;
}

// Clean data
const cleanData = (obj:any) => {
    Object.keys(obj).forEach(k =>
        (obj[k] && typeof obj[k] === 'object') && cleanData(obj[k]) ||
        (!obj[k] && obj[k] !== undefined) && delete obj[k]
    );
    obj = removeUnsafe(obj);
    return obj;
};

export {
    es,
    cleanData
};