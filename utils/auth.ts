import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import moment from 'moment-timezone';
import * as shortid from 'shortid';
import { IHuddlUser, IError } from '../interfaces';
import { cache } from './cache';

const saltRounds = 10;
const codeLength = 32;
const codeType = 'base64';
/// TS Secret
const HUDDLS_JWT_SECRET = process.env.HUDDLS_JWT_SECRET!;
const JITSI_JWT_SECRET = process.env.JITSI_JWT_SECRET!;

interface AuthToken {
    token: string;
    refresh: string;
}

class Authentication {
    validateLoginToken(token:string):any
    {
        try {
            let data:any = jwt.verify(token, HUDDLS_JWT_SECRET);
            if(data)
            {
                if(parseInt(data.exp) >= moment().unix() && data.type !== "request")
                    return data;
                console.log("Token expired?", data);
                console.log("Moment Time: ", moment().unix());
                console.log("My Exp: ", parseInt(data.exp));
                }
        } catch (error) {
            console.log(error);
        }
        return null;
    }
	// Refresh token
    async refreshLoginToken(refresh_token:string)
    {
        try {
            let data:any = jwt.verify(refresh_token, HUDDLS_JWT_SECRET);
            console.log(data);
            if(data.token)
            {
                var uuid = await cache.get(`_refresh/${data.token}`);
                if(uuid)    
                {
                    cache.delete(`_refresh/${data.token}`);
                    return {uuid, error:null};
                } else {
                    console.log("Could not find refresh token in cache...");
                    return {error: "Unable to get refresh token"};
                }
            } else {
                console.log("Invalid token, no token.");
                return {error: "Invalid Refresh Token"};
            }
        } catch(error) {
            console.log("Invalid token: ", error);
            return {error: "Invalid Refresh Token"};
        }
    }

    createLoginToken(user:IHuddlUser):any
    {
        try {
            // This seems wrong... someone should fix it.
            var refreshToken = `${shortid.generate()}.${shortid.generate()}.${shortid.generate()}`;
            cache.set(`_refresh/${refreshToken}`, `${user.uuid}`, 2592000000);
            var refresh_data = {
                token: refreshToken,
                type: "refresh",
                iss: "huddls.com",
                aud: "huddls",
                sub: "user",
                exp: moment().unix() + 2592000, // 30 Days
                auth_time: moment().unix()
            }
            var data = {
                uuid: user.uuid,
                type: user.type,
                iss: "huddls.com",
                aud: "huddls",
                sub: "user",
                exp: moment().unix() + 900, // 15 minutes
                auth_time: moment().unix()
            }
            return {token: jwt.sign(data, HUDDLS_JWT_SECRET, {}), refresh: jwt.sign(refresh_data, HUDDLS_JWT_SECRET)};
        } catch (error){
            console.error(error);
        }
    }

    decodeCommonJWT(token:string)
    {
        try {
            let data:any = jwt.verify(token,HUDDLS_JWT_SECRET);
            return data.payload;
        } catch(error) {
            console.log(error);
            return null;
        }
    }

    decodeActionToken(token:string)
    {
        try {
            let data:any = jwt.verify(token,HUDDLS_JWT_SECRET);
            return data;
        } catch(error) {
            console.log(error);
            return null;
        }
    }

    encodeActionToken(action:string, type:string, payload:any)
    {
        try {
            let token = jwt.sign({action: action, type: type, payload: payload},HUDDLS_JWT_SECRET);
            return token;
        } catch(error) {
            console.log(error);
            return null;
        }
    }

    encodeCommonJWT(payload:any)
    {
        try {
            let token = jwt.sign({payload: payload},HUDDLS_JWT_SECRET);
            return token;
        } catch(error) {
            console.log(error);
            return null;
        }
    }

    generateMeetToken(user:IHuddlUser, room:string, moderator:boolean, expires = null)
    {
        var data = {
            context: {
              user: {
                avatar: `https://static.huddls.com${user.profile_pic!}`,
                name: `${user.first_name!} ${user.last_name!}`,
                id: user.uuid!
              },
              group: user.type
            },
            moderator: moderator,
            aud: "meet.huddls.com",
            iss: "meet.huddls.com",
            sub: "meet.huddls.com",
            room: room,
            expires: moment().add(12, 'hours').unix()
        };
        console.log(data);
        let token = jwt.sign(data, JITSI_JWT_SECRET);
        return token;
    }

    async generateHash(password:string) 
    {
        var hash = await bcrypt.hash(password, saltRounds);
        return hash;
    }

    async checkHash(clearPassword:string, hashedPassword:string)
    {
        var success = await bcrypt.compare(clearPassword, hashedPassword);
        return success;
    }

    generateCode()
    {
        return `${shortid.generate()}.${shortid.generate()}`;
    }
}

export const auth = new Authentication();